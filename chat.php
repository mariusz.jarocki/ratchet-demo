<?php

    use Ratchet\MessageComponentInterface;
    use Ratchet\ConnectionInterface;

    require __DIR__ . '/vendor/autoload.php';

    class MyChat implements MessageComponentInterface {
        protected $clients;

        public function __construct() {
            $this->clients = new \SplObjectStorage;
        }

        public function onOpen(ConnectionInterface $conn) {
            $this->clients->attach($conn);
        }

        public function onMessage(ConnectionInterface $from, $msg) {
            foreach ($this->clients as $client) {
                if ($from != $client) {
                    $client->send($msg);
                }
            }
        }

        public function onClose(ConnectionInterface $conn) {
            $this->clients->detach($conn);
        }

        public function onError(ConnectionInterface $conn, \Exception $e) {
            $conn->close();
        }
    }

    $app = new Ratchet\App('localhost', 8880);
    // odsyłanie wiadomości do tej samej sesji ws (echo)
    $app->route('/echo', new Ratchet\Server\EchoServer, array('*'));
    // rozsyłanie wiadomości do innych sesji ws
    $app->route('/chat', new MyChat, array('*'));
    $app->run();

?>