# ratchet-demo

## konfiguracja apache

``a2enmod proxy proxy_http proxy_wstunnel``

    ProxyPassMatch ^/ws/(.*) ws://localhost:8880/$1
    ProxyPassReverse ^/ws/(.*) ws://localhost:8880/$1

``service apache2 restart``

## instalacja composera

``apt install composer``

## inicjalizacja

``composer init``

## uruchomienie

``php chat.php``

## js code

    let conn = new WebSocket('ws://' + window.location.host + '/ws/chat')
    // handler
    conn.onmessage = function(e) { console.log('MESSAGE', e.data) }
    // sender
    conn.send('Hello World')

## angularjs code

    // npm install angular-ws
    let app = angular.module('app', ['ws'])
    app.config(['wsProvider', function(wsProvider) {
        wsProvider.setUrl('ws://' + window.location.host + '/ws/chat')
    }])
    ...
    // handler, service ws
    ws.on('message', function(message) { ... }
    // sender
    ws.send(...)